<?php
/**
 * Created by PhpStorm.
 * User: minfire
 * Date: 28/1/19
 * Time: 3:01 PM
 */

namespace Tests\AppBundle\Constants;


final class CommandConstants
{
    public const COMMAND_EXECUTE = [
        'success' => 'CSV Imported Successfully!',
        'name' => 'csv-import'
    ];
}