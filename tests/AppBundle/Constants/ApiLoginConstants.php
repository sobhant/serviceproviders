<?php
/**
 * Created by PhpStorm.
 * User: minfire
 * Date: 28/1/19
 * Time: 11:15 AM
 */

namespace Tests\AppBundle\Constants;


final class ApiLoginConstants
{
    public const LOGIN_ACTION = [
        'method' => 'GET',
        'uri' => '/v1/api/GetToken',
        'header' => ['CONTENT_TYPE' => 'application/json; charset=UTF-8'],
    ];

    public const USER_CREATE = [
        'username' => 'Karan',
        'email' => 'karan@gmail.com',
        'password' => 'password',
        'api_token' => 'b03f21062540ea53'
    ];

    public const INVALID_USER = [
        'username' => 'Sobhan'
    ];
}