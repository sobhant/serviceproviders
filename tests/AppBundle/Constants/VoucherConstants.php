<?php
/**
 * Created by PhpStorm.
 * User: minfire
 * Date: 28/1/19
 * Time: 11:54 AM
 */

namespace Tests\AppBundle\Constants;


final class VoucherConstants
{
    public const CREATE_VOUCHER =[
        'expiry_date' => '30-10-2018'
    ];

    public const CREATE_ACTION = [
        'header' => ['CONTENT_TYPE' => 'application/json; charset=UTF-8'],
        'method' => 'POST',
        'uri' => '/v1/api/vouchers',
        'content' => [
            'expiry_date' => '2019-10-30'
        ]
    ];

    public const EMPTY_FIELDS = [
        'Error' => 'Fields Cannot be Empty'
    ];

    public const REDEEM_ACTION = [
        'header' => ['CONTENT_TYPE' => 'application/json; charset=UTF-8'],
        'method' => 'PUT',
        'uri' => '/v1/api/redeem',
        'content' => [
            'expiry_date' => '2019-10-30'
        ]
    ];

    public const EXPIRED_VOUCHER = [
        'status' => 'expired',
        'expiry_date' => '2019-10-30',
        'Error' => 'Voucher Expired'
    ];

    public const REDEEMED_VOUCHER = [
        'status' => 'redeem',
        'expiry_date' => '2019-10-30',
        'Error' => 'Voucher Cannot be Re-Used'
    ];

    public const INVALID_VOUCHER = [
        'voucher_id' => '12345678',
        'Error' => 'No Such Voucher Present'
    ];
}