<?php
/**
 * Created by PhpStorm.
 * User: minfire
 * Date: 28/1/19
 * Time: 11:51 AM
 */

namespace Tests\AppBundle\Constants;


final class ServiceProviderConstants
{
    public const SERVICE_PROVIDER_CREATE = [
      'name' => 'Big Bazaar',
      'limit' => '10'
    ];

    public const CREATE_ACTION = [
        'header' => ['CONTENT_TYPE' => 'application/json; charset=UTF-8'],
        'method' => 'POST',
        'uri' => '/v1/api/serviceprovider',
        'content' => [
            'name' => 'Max Brands',
            'limit' => '10'
        ]
    ];

    public const EMPTY_ACTION = [
        'header' => ['CONTENT_TYPE' => 'application/json; charset=UTF-8'],
        'method' => 'POST',
        'uri' => '/v1/api/serviceprovider',
        'content' => [
            'name' => 'Max Brands',
        ]
    ];

    public const EMPTY_ERROR = [
        'Error' => 'Fields Cannot be Empty'
    ];

    public const LIMITED_VOUCHER = [
        'header' => ['CONTENT_TYPE' => 'application/json; charset=UTF-8'],
        'method' => 'POST',
        'uri' => '/v1/api/serviceprovider',
        'content' => [
            'name' => 'Max Brands',
            'limit' => '25'
        ]
    ];

    public const INVALID_SERVICE_PROVIDER = [
        'username' => 'Sobhan',
        'email' => 'sobhan@gmail.com',
        'password' => 'password',
        'api_token' => 'b03f21062540ea53'
    ];

}