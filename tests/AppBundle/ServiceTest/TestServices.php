<?php
/**
 * Created by PhpStorm.
 * User: minfire
 * Date: 17/1/19
 * Time: 1:05 PM
 */

namespace Tests\AppBundle\ServiceTest;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Task\ProjectBundle\Entity\ServiceProvider;
use Task\ProjectBundle\Entity\User;
use Task\ProjectBundle\Entity\Vouchers;
use Task\ProjectBundle\Service\CSVImport;
use Task\ProjectBundle\Service\Validate;
use Doctrine\ORM\EntityManagerInterface;
use Tests\AppBundle\Constants\ApiLoginConstants;
use Tests\AppBundle\Constants\ServiceProviderConstants;
use Tests\AppBundle\Constants\VoucherConstants;

class TestServices extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;
    private $validateMock;
    private $token;
    private $user;
    private $serviceProvider;
    private $voucher;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
        $userRepository = $this->entityManager
            ->getRepository(User::class);
        $result = $userRepository->findOneBy(array('username'=>ApiLoginConstants::USER_CREATE['username']));
        if(!$result) {
            $this->user = new User();
            $this->user->setUsername(ApiLoginConstants::USER_CREATE['username']);
            $this->user->setEmail(ApiLoginConstants::USER_CREATE['email']);
            $this->user->setPlainPassword(ApiLoginConstants::USER_CREATE['password']);
            $this->user->setEnabled(true);
            $this->token = ApiLoginConstants::USER_CREATE['api_token'];
            $this->user->setApiToken($this->token);
            $this->entityManager->persist($this->user);
            $this->entityManager->flush();
        } else {
            $this->user = $result;
            $this->token=$result->getApiToken();
        }
    }

    public function testValidUserName()
    {
        $validate=new Validate($this->entityManager);
        $result=$validate->validateUserCredentials($this->user->getUsername());
        $result1=$validate->validateUserCredentials(ApiLoginConstants::INVALID_USER['username']);
        $this->assertNotNull($result);
        $this->assertNull($result1);
    }

    public function testValidToken()
    {
        $validate=new Validate($this->entityManager);
        $result=$validate->validateToken($this->token);
        $result1=$validate->validateToken(openssl_random_pseudo_bytes(8));
        $this->assertNotNull($result);
        $this->assertNull($result1);
    }

    public function testCreateServiceProvider()
    {
        $validate=new Validate($this->entityManager);
        $this->serviceProvider=$validate->PostServiceProvider($this->user,ServiceProviderConstants::SERVICE_PROVIDER_CREATE['name'],ServiceProviderConstants::SERVICE_PROVIDER_CREATE['limit']);
        if($this->serviceProvider) {
            $this->assertInstanceOf(ServiceProvider::class,$this->serviceProvider);
        }
    }

    public function testValidateServiceProvider()
    {
        $validate=new Validate($this->entityManager);
        $result = $validate->validateServiceProvider($this->user);
        if ($result) {
            $this->assertInstanceOf(ServiceProvider::class,$result);
        } else {
            $this->assertNull($result);
        }
    }

    public function testCreateVoucher()
    {
        $validate=new Validate($this->entityManager);
        $this->voucher = $validate->PostVoucher($this->user,$this->serviceProvider,VoucherConstants::CREATE_VOUCHER['expiry_date']);
        if($this->voucher) {
            $this->assertInstanceOf(Vouchers::class,$this->voucher);
        } else {
            $this->assertNull($this->voucher);
        }
    }

    public function testRedeemedVoucher()
    {
        $redeemedVoucher = new Vouchers();
        $redeemedVoucher->setStatus(VoucherConstants::REDEEMED_VOUCHER['status']);
        $redeemedVoucher->setExpiryDate(VoucherConstants::REDEEMED_VOUCHER['expiry_date']);
        $redeemedVoucher->setUser($this->user);
        $validate=new Validate($this->entityManager);
        $error = $validate->RedeemVoucher($redeemedVoucher);
        $this->assertEquals(VoucherConstants::REDEEMED_VOUCHER['Error'],$error['Error']);
    }

    public function testExpiredVoucher()
    {
        $expiredVoucher = new Vouchers();
        $expiredVoucher->setStatus(VoucherConstants::EXPIRED_VOUCHER['status']);
        $expiredVoucher->setExpiryDate(VoucherConstants::EXPIRED_VOUCHER['expiry_date']);
        $expiredVoucher->setUser($this->user);
        $validate=new Validate($this->entityManager);
        $error = $validate->RedeemVoucher($expiredVoucher);
        $this->assertEquals(VoucherConstants::EXPIRED_VOUCHER['Error'],$error['Error']);
    }

    public function testInvalidServiceProvider()
    {
        $user = new User();
        $user->setApiToken(ServiceProviderConstants::INVALID_SERVICE_PROVIDER['api_token']);
        $user->setEmail(ServiceProviderConstants::INVALID_SERVICE_PROVIDER['email']);
        $user->setPlainPassword(ServiceProviderConstants::INVALID_SERVICE_PROVIDER['password']);
        $user->setUsername(ServiceProviderConstants::INVALID_SERVICE_PROVIDER['username']);
        $validate=new Validate($this->entityManager);
        $error = $validate->validateServiceProvider($user);
        $this->assertNull($error);
    }

    public function testInvalidVoucher()
    {
        $validate=new Validate($this->entityManager);
        $error = $validate->findVoucher(VoucherConstants::INVALID_VOUCHER['voucher_id'],$this->user);
        $this->assertNull($error);
    }

    /**
     * {@inheritDoc}
     */
    public function tearDown()
    {
        parent::tearDown();
        $this->entityManager->close();
        $this->entityManager = null; // avoid memory leaks
        $this->validateMock=null;
    }
}