<?php
/**
 * Created by PhpStorm.
 * User: minfire
 * Date: 22/1/19
 * Time: 12:05 PM
 */

namespace Tests\AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\Console\Application;
use Task\ProjectBundle\Command\CsvImportCommand;
use Task\ProjectBundle\Service\CSVImport;
use Task\ProjectBundle\Service\Validate;
use Tests\AppBundle\Constants\CommandConstants;

class TestCommand extends KernelTestCase
{
    private $entityManager;
    private $validate;
    private $csvImport;
    private $csvImportCommand;
    private $commandTester;
    private $command;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
        $this->validate=new Validate($this->entityManager);
        $this->csvImport=new CSVImport($this->entityManager,$this->validate);
        $this->csvImportCommand=new CsvImportCommand($this->csvImport);
        $application = new Application();
        $application->add(new CsvImportCommand($this->csvImport));
        $this->command = $application->find(CommandConstants::COMMAND_EXECUTE['name']);
        $this->commandTester = new CommandTester($this->command);
    }

    public function tearDown()
    {
        $this->entityManager=null;
        $this->validate=null;
        $this->csvImport=null;
        $this->csvImportCommand=null;
    }

    public function testExecute()
    {
        $this->commandTester->execute(array('command' => $this->command->getName()));
        $output = $this->commandTester->getDisplay();
        $this->assertContains(CommandConstants::COMMAND_EXECUTE['success'],$output);
    }

    public function invalidTestExecute()
    {

    }
}