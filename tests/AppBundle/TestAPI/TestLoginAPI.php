<?php
/**
 * Created by PhpStorm.
 * User: minfire
 * Date: 28/1/19
 * Time: 12:05 PM
 */

namespace Tests\AppBundle\TestAPI;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Tests\AppBundle\Constants\ApiLoginConstants;

class TestLoginAPI extends WebTestCase
{
    public function testLogin()
    {
        $header = ApiLoginConstants::LOGIN_ACTION['header'];
        $method = ApiLoginConstants::LOGIN_ACTION['method'];
        $uri = ApiLoginConstants::LOGIN_ACTION['uri'];

        $response = $this->getResponse($method, $uri, [], [], $header,[]);

        $responseContent = json_decode($response->getContent(), true);
        if($responseContent['Token']) {
            $this->token=$responseContent['Token'];
            $this->assertEquals(200,$response->getStatusCode());
        } else {
            $this->assertEquals(500,$response->getStatusCode());
        }
    }

    public function getResponse($method, $uri, $parameters = [], $files = [], $header = [], $content = [], $changeHistory = true)
    {
        $client = static::createClient([],['PHP_AUTH_USER'=>'Karan', 'PHP_AUTH_PW'=>'password'
        ]);
        $host = $client->getKernel()->getContainer()->getParameter('api_host');
        $client->setServerParameter('HTTP_HOST', $host );
        $client->request($method, $uri, $parameters, $files, $header, $content, $changeHistory);

        return $client->getResponse();
    }

}