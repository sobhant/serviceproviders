<?php
/**
 * Created by PhpStorm.
 * User: minfire
 * Date: 18/1/19
 * Time: 1:25 PM
 */

namespace Tests\AppBundle\TestAPI;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Tests\AppBundle\Constants\ApiLoginConstants;
use Task\ProjectBundle\Entity\User;
use Tests\AppBundle\Constants\ServiceProviderConstants;

class TestServiceProviderControllers extends WebTestCase
{
    private $token;
    private $entityManager;
    public function setUp()
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
        $userRepository = $this->entityManager
            ->getRepository(User::class);
        $result = $userRepository->findOneBy(array('username'=>ApiLoginConstants::USER_CREATE['username']));
        $this->token=$result->getApiToken();
    }

    public function testPostServiceProvider()
    {
        $header = array_merge(ServiceProviderConstants::CREATE_ACTION['header'],array('api_token'=>$this->token));
        $method = ServiceProviderConstants::CREATE_ACTION['method'];
        $uri = ServiceProviderConstants::CREATE_ACTION['uri'];
        $content=json_encode(ServiceProviderConstants::CREATE_ACTION['content']);
        $response = $this->getResponse($method, $uri, [], [], $header,$content);

        $responseContent = json_decode($response->getContent(), true);

        if($responseContent['Status']) {
            $this->assertEquals(200,$response->getStatusCode());
        } else {
            $this->assertEquals(500,$response->getStatusCode());
        }
    }

    public function testPostServiceProviderWithEmptyData()
    {
        $header = array_merge(ServiceProviderConstants::EMPTY_ACTION['header'],array('api_token'=>$this->token));
        $method = ServiceProviderConstants::EMPTY_ACTION['method'];
        $uri = ServiceProviderConstants::EMPTY_ACTION['uri'];
        $content=json_encode(ServiceProviderConstants::EMPTY_ACTION['content']);

        $response = $this->getResponse($method, $uri, [], [], $header,$content);

        $responseContent = json_decode($response->getContent(), true);
        $this->assertEquals(ServiceProviderConstants::EMPTY_ERROR['Error'],$responseContent['Error']);
    }

    public function testVoucherLimitForServiceProvider()
    {
        $header = array_merge(ServiceProviderConstants::LIMITED_VOUCHER['header'],array('api_token'=>$this->token));
        $method = ServiceProviderConstants::LIMITED_VOUCHER['method'];
        $uri = ServiceProviderConstants::LIMITED_VOUCHER['uri'];
        $content=json_encode(ServiceProviderConstants::LIMITED_VOUCHER['content']);

        $response = $this->getResponse($method, $uri, [], [], $header,$content);

        $this->assertEquals(500,$response->getStatusCode());
    }

    public function getResponse($method, $uri, $parameters = [], $files = [], $header = [], $content = [], $changeHistory = true)
    {
        $client = static::createClient();
        $host = $client->getKernel()->getContainer()->getParameter('api_host');
        $client->setServerParameter('HTTP_HOST', $host );
        $client->request($method, $uri, $parameters, $files, $header, $content, $changeHistory);

        return $client->getResponse();
    }

    public function tearDown()
    {
        $this->token=null;
    }
}