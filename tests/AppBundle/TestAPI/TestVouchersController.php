<?php
/**
 * Created by PhpStorm.
 * User: minfire
 * Date: 23/1/19
 * Time: 4:00 PM
 */

namespace Tests\AppBundle\TestAPI;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Tests\AppBundle\Constants\VoucherConstants;
use Task\ProjectBundle\Entity\User;
use Tests\AppBundle\Constants\ApiLoginConstants;

class TestVouchersController extends WebTestCase
{
    private $token;
    private static $voucherID;
    public function setUp()
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
        $userRepository = $this->entityManager
            ->getRepository(User::class);
        $result = $userRepository->findOneBy(array('username'=>ApiLoginConstants::USER_CREATE['username']));
        $this->token=$result->getApiToken();
    }

    public function testPostVoucher()
    {
        $header = array_merge(VoucherConstants::CREATE_ACTION['header'],array('api_token'=>$this->token));
        $method = VoucherConstants::CREATE_ACTION['method'];
        $uri = VoucherConstants::CREATE_ACTION['uri'];
        $content=json_encode(VoucherConstants::CREATE_ACTION['content']);

        $response = $this->getResponse($method, $uri, [], [], $header,$content);

        $result = json_decode($response->getContent(),true);
        self::$voucherID = $result['Success']['voucherID'];
        if($response->getStatusCode() === 200) {
            $this->assertEquals(200,$response->getStatusCode());
        } else {
            $this->assertEquals(500,$response->getStatusCode());
        }
    }

    public function testEmptyVoucherExpiryDate()
    {
        $header = array_merge(VoucherConstants::CREATE_ACTION['header'],array('api_token'=>$this->token));
        $method = VoucherConstants::CREATE_ACTION['method'];
        $uri = VoucherConstants::CREATE_ACTION['uri'];
        $content=json_encode(array());
        $response = $this->getResponse($method, $uri, [], [], $header,$content);
        $result = json_decode($response->getContent(),true);
        $this->assertContains(VoucherConstants::EMPTY_FIELDS['Error'],$result['Error']);
    }

    public function testRedeemVoucher()
    {
        $voucherID = self::$voucherID;
        $header = array_merge(VoucherConstants::REDEEM_ACTION['header'],array('api_token'=>$this->token));
        $method = VoucherConstants::REDEEM_ACTION['method'];
        $uri = VoucherConstants::REDEEM_ACTION['uri'];
        $content=json_encode(array('voucherID'=>$voucherID));
        $response = $this->getResponse($method, $uri, [], [], $header,$content);
        $this->assertEquals(200,$response->getStatusCode());
    }

    public function testInvalidVoucher()
    {
        $header = array_merge(VoucherConstants::REDEEM_ACTION['header'],array('api_token'=>$this->token));
        $method = VoucherConstants::REDEEM_ACTION['method'];
        $uri = VoucherConstants::REDEEM_ACTION['uri'];
        $content=json_encode(array('voucherID'=>VoucherConstants::INVALID_VOUCHER['voucher_id']));
        $response = $this->getResponse($method, $uri, [], [], $header,$content);
        $result = json_decode($response->getContent(),true);
        $this->assertEquals(VoucherConstants::INVALID_VOUCHER['Error'],$result['Error']);
    }

    public function getResponse($method, $uri, $parameters = [], $files = [], $header = [], $content = [], $changeHistory = true)
    {
        $client = static::createClient();
        $host = $client->getKernel()->getContainer()->getParameter('api_host');
        $client->setServerParameter('HTTP_HOST', $host );
        $client->request($method, $uri, $parameters, $files, $header, $content, $changeHistory);

        return $client->getResponse();
    }

}