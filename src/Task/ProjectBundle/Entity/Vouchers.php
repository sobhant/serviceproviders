<?php

namespace Task\ProjectBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;

/**
 * Vouchers
 *
 * @ORM\Table(name="Vouchers",indexes={@Index(name="voucher_index", columns={"voucher_id"})})
 * @ORM\Entity(repositoryClass="Task\ProjectBundle\Repository\VouchersRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Vouchers
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=20)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expiry_date", type="datetime")
     */
    private $expiry_date;

    /**
     * @var string
     *
     * @ORM\Column(name="voucher_id", type="string", length=30)
     */
    private $voucher_id;

    /**
     * @var string
     *
     * @ORM\Column(name="remark", type="string", length=500)
     */
    private $remark;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(
     * targetEntity="ServiceProvider")
     * @ORM\JoinColumn(name="ServiceProvider_ID")
     */
    protected $serviceProvider_ID;

    /**
     * @ORM\ManyToOne(
     * targetEntity="user")
     * @ORM\JoinColumn(name="user_id")
     */
    protected $user;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Vouchers
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set expiry_date
     *
     * @param \DateTime $expiry_date
     *
     * @return Vouchers
     */
    public function setExpiryDate($expiry_date)
    {
        $this->expiry_date = $expiry_date;

        return $this;
    }

    /**
     * Get expiry_date
     *
     * @return \DateTime
     */
    public function getExpiryDate()
    {
        return $this->expiry_date;
    }

    /**
     * Set voucher_id
     *
     * @param string $voucher_id
     *
     * @return Vouchers
     */
    public function setVoucherId($voucher_id)
    {
        $this->voucher_id = $voucher_id;

        return $this;
    }

    /**
     * Get voucher_id
     *
     * @return string
     */
    public function getVoucherId()
    {
        return $this->voucher_id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Vouchers
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Vouchers
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setUpdatedAt(new \DateTime('now'));

        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }

    /**
     * Set serviceProviderID
     *
     * @param \Task\ProjectBundle\Entity\ServiceProvider $serviceProviderID
     *
     * @return Vouchers
     */
    public function setServiceProviderID(\Task\ProjectBundle\Entity\ServiceProvider $serviceProviderID = null)
    {
        $this->serviceProvider_ID = $serviceProviderID;

        return $this;
    }

    /**
     * Get serviceProviderID
     *
     * @return \Task\ProjectBundle\Entity\ServiceProvider
     */
    public function getServiceProviderID()
    {
        return $this->serviceProvider_ID;
    }

    /**
     * Set user
     *
     * @param \Task\ProjectBundle\Entity\user $user
     *
     * @return Vouchers
     */
    public function setUser(\Task\ProjectBundle\Entity\user $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Task\ProjectBundle\Entity\user
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set remark.
     *
     * @param string $remark
     *
     * @return Vouchers
     */
    public function setRemark($remark)
    {
        $this->remark = $remark;

        return $this;
    }

    /**
     * Get remark.
     *
     * @return string
     */
    public function getRemark()
    {
        return $this->remark;
    }
}
