<?php
namespace Task\ProjectBundle\Entity;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;

/**
 * @ORM\Entity
 * @ORM\Table(name="user",indexes={@Index(name="search_index", columns={"api_token", "username"})})
 * @ORM\HasLifecycleCallbacks
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var datetime $created_at
     * @ORM\Column(type="datetime")
     */
    protected $created_at;

    /**
     * @var datetime $updated_at
     * @ORM\Column(type="datetime")
     */
    protected $updated_at;

    /**
     * @var string $api_token
     * @ORM\Column(type="string", nullable=true)
     */
    protected $api_token;

    /**
     * @ORM\ManyToOne(targetEntity="ServiceProvider")
     * @ORM\JoinColumn(name="ServiceProvider_ID")
     */
    protected $sp;

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setUpdatedAt(new \DateTime('now'));

        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }

    /**
     * Set createdAt
     * @param \DateTime $createdAt
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updatedAt
     * @param \DateTime $updatedAt
     * @return User
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set apiToken
     * @param string $apiToken
     * @return User
     */
    public function setApiToken($apiToken)
    {
        $this->api_token = $apiToken;

        return $this;
    }

    /**
     * Get apiToken
     * @return string
     */
    public function getApiToken()
    {
        return $this->api_token;
    }

    /**
     * Set sp
     *
     * @param \Task\ProjectBundle\Entity\ServiceProvider $sp
     *
     * @return User
     */
    public function setSp(\Task\ProjectBundle\Entity\ServiceProvider $sp = null)
    {
        $this->sp = $sp;

        return $this;
    }

    /**
     * Get sp
     *
     * @return \Task\ProjectBundle\Entity\ServiceProvider
     */
    public function getSp()
    {
        return $this->sp;
    }
}
