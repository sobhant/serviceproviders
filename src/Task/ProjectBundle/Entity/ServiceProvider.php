<?php

namespace Task\ProjectBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ServiceProvider
 *
 * @ORM\Table(name="service_provider")
 * @ORM\Entity(repositoryClass="Task\ProjectBundle\Repository\ServiceProviderRepository")
 * @ORM\HasLifecycleCallbacks
 */
class ServiceProvider
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="serviceProvider_Name", type="string", length=255)
     */
    private $serviceProvider_Name;

    /**
     * @var string
     * @ORM\Column(name="serviceProvider_ID", type="string", length=255)
     */
    private $serviceProvider_ID;

    /**
     * @var int
     * @ORM\Column(name="voucher_limit", type="integer")
     */
    private $voucher_limit;

    /**
     * @var \DateTime
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(
     * targetEntity="User")
     * @ORM\JoinColumn(name="createdBy")
     */
    protected $created_by;


    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set serviceProvider_Name
     * @param string $serviceProvider_Name
     * @return ServiceProvider
     */
    public function setServiceProviderName($serviceProvider_Name)
    {
        $this->serviceProvider_Name = $serviceProvider_Name;

        return $this;
    }

    /**
     * Get serviceProvider_Name
     * @return string
     */
    public function getServiceProviderName()
    {
        return $this->serviceProvider_Name;
    }

    /**
     * Set serviceProvider_ID
     * @param string $serviceProvider_ID
     * @return ServiceProvider
     */
    public function setServiceProviderID($serviceProvider_ID)
    {
        $this->serviceProvider_ID = $serviceProvider_ID;

        return $this;
    }

    /**
     * Get serviceProvider_ID
     * @return string
     */
    public function getServiceProviderID()
    {
        return $this->serviceProvider_ID;
    }

    /**
     * Set voucher_limit
     * @param integer $voucher_limit
     * @return ServiceProvider
     */
    public function setVoucherLimit($voucher_limit)
    {
        $this->voucher_limit = $voucher_limit;

        return $this;
    }

    /**
     * Get voucher_limit
     * @return int
     */
    public function getVoucherLimit()
    {
        return $this->voucher_limit;
    }

    /**
     * Set createdAt
     * @param \DateTime $createdAt
     * @return ServiceProvider
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     * @param \DateTime $updatedAt
     * @return ServiceProvider
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function __toString() 
    {
        if(is_null($this->serviceProvider_Name)) {
        return 'NULL';
        }
        return $this->serviceProvider_Name;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setUpdatedAt(new \DateTime('now'));

        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }

    /**
     * Set createdBy
     * @param \Task\ProjectBundle\Entity\User $createdBy
     * @return ServiceProvider
     */
    public function setCreatedBy(\Task\ProjectBundle\Entity\User $createdBy = null)
    {
        $this->created_by = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     * @return \Task\ProjectBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }
}
