<?php
namespace Task\ProjectBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManager;
use Task\ProjectBundle\Service\Validate;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Psr\Log\LoggerInterface;

class RequestListener
{
    protected $validate;
    protected $container;
    protected $log;
    public function __construct(Validate $validate, Container $container,  LoggerInterface $log)
    {
        $this->validate = $validate;
        $this->container = $container;
        $this->log = $log;
    }
    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        $route = $request->attributes->get('_route');

        $link = $request->getUri();
        $request_log = array('URL' => $link);

        // An Array to define set of routes, so that the listener 
        // will only allow these routes which are present inside the array

        $routed_array = array("task_project_v1_user_createtoken_generatetoken",
            "task_project_v1_serviceprovider_createserviceprovider_postserviceprovider",
            "task_project_v1_vouchers_createvoucher_createvouchers",
            "task_project_v1_vouchers_redeemvoucher_redeemvouchers","task_project_v1_serviceprovider_fetchserviceproviders_fetchserviceproviders", "task_project_v1_vouchers_fetchvouchers_fetchvouchers");

        // Response Object
        $response = new Response();
        $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);

        if(in_array($route, $routed_array))
        {
            if($route == 'task_project_v1_user_createtoken_generatetoken')
            {
                $request_log = array_merge($request_log,array('Header_Type'=>'Basic Auth'));
                // If the request is GetToken, Then validate the Basic Auth Header.
                $username = $request->server->get('PHP_AUTH_USER');
                $password = $request->server->get('PHP_AUTH_PW');

                $request_log = array_merge($request_log,array('Username'=>$username));
                $request_log = array_merge($request_log,array('Password'=>$password));

                $this->log->info('Log', array("Log Message" => $request_log));

                $request->attributes->set('request_log', $request_log);

                if (empty($username) || empty($password))
                {
                    $response ->setContent(json_encode(array("Error" => "Headers cannot be blank")));
                    $event->setResponse($response);
                    return;
                }
               
                //Call the service to check whether the header input is correct or not
                $user = $this->validate->validateUserCredentials($username);
                if(!$user) {
                    $response ->setContent(json_encode(array("Error" => "Invalid Header Information")));
                    $event->setResponse($response);
                    return;
                }
                $encoderService = $this->container->get('security.password_encoder');
                $match = $encoderService->isPasswordValid($user, $password,$user->getSalt() ? "true" : "false");
            
                // If the credential is wrong
                if(!$match)
                {
                    $response ->setContent(json_encode(array("Error" => "Invalid Header Information")));
                    $event->setResponse($response);
                    return;
                }

                // Set the user object in user_object key.
                $request->attributes->set('user_object', $user);
            } 

            else
            {
                $request_log = array_merge($request_log,array('Header_Type'=>'Api_Token'));
                if($request->server->get('api_token')){
                    $api_token = $request->server->get('api_token');
                } else {
                    $api_token = $request->headers->get('api_token');
                }

                $request_log = array_merge($request_log,array('Token'=>$api_token));
                $request_log = array_merge($request_log,array('Content'=>json_decode($request->getContent(),true)));
                $request->attributes->set('request_log', $request_log);

                $this->log->info('Log', array("Log Message" => $request_log));
                
                // Call the validate service and verify for api_token header.
                $user = $this->validate->validateToken($api_token);

                if(!$user)
                {
                    $response ->setContent(json_encode(array("Error" => "Invalid Token")));
                    $event->setResponse($response);
                    return;
                }
                
                // Set the user object in user_object key.
                $request->attributes->set('user_object', $user);
            }
        }
    }
}