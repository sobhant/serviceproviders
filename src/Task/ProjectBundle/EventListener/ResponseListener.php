<?php
namespace Task\ProjectBundle\EventListener;

use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Psr\Log\LoggerInterface;


class ResponseListener
{
	private $container;
    protected $log;
	
	public function __construct(Container $container, LoggerInterface $log) {
		$this->container = $container;
        $this->log = $log;
	}

    public function onKernelResponse(FilterResponseEvent $event)
    {
    	$response = $event->getResponse();
        $content = $response->getContent();

        $request   = $event->getRequest();
        $log_request = $request->get('request_log');

        $code = $response->getStatusCode();
        /*
            Call monolog.logger.response Service and write the logs to response.log File
        */
        if(!empty($log_request)) {
            if($code == 200) {
            $this->log->info('Log', array("Request" => $log_request, "Response" => json_decode($content,true)));
            } else {
                $this->log->error('Log', array("Request" => $log_request, "Response" => json_decode($content,true)));
            }
        }
    }    
}