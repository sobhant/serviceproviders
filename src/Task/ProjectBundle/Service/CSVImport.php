<?php
namespace Task\ProjectBundle\Service;

use Doctrine\ORM\EntityManager;
use Task\ProjectBundle\Entity\ServiceProvider;
use League\Csv\Reader;
use Task\ProjectBundle\Service\Validate;


class CSVImport
{
    protected $em;
    protected $validate;

	public function __construct(EntityManager $entityManager, Validate $validate)
	{
	    $this->em = $entityManager;
	    $this->validate = $validate;
	}

	public function ServiceProviderImport()
	{
        /*
            Initiliase a loop counter to 0.
            This loop counter will be used as the index to store the invalid records
            in a temporary array.
        */

        $i = 0;

        try
        {
            // Set the file type to Csv.
            $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Csv');

            // Set the file mode to READONLY.
            $reader->setReadDataOnly(TRUE);

            // Load the file.
            $spreadsheet = $reader->load('src/Task/ProjectBundle/Data/ServiceProvider.csv');

            $worksheet = $spreadsheet->getActiveSheet();

             // Get the highest row and column numbers referenced in the worksheet
            $highestRow = $worksheet->getHighestRow(); 
            $highestColumn = $worksheet->getHighestColumn(); 

            $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn); 

            for ($row = 2; $row <= $highestRow; ++$row) 
            {
                // initialise an array to store the results.
                $result = array();

                for ($col = 1; $col <= $highestColumnIndex; ++$col) 
                {
                    $value = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                    $result[$col] = $value;
                }

                // Keep the records in separate variables.

                $name = $result[1];
                $voucher_limit = $result[2];
                $user_id = $result[3];

                /*
                    Validate the csv file. 
                    If the user_id of the csv file is not an integer or service provider name length is 
                    greater than 50 or the voucher limit is greater than 30, then keep that 
                    record in a temporary array and write that record into an another csv file.
                */

                if(((int)$user_id != 0) && (strlen($name) <= 50) && ((int)$voucher_limit <= 30))
                {
                    // Create new Service Provider
                    $ServiceProvider = (new ServiceProvider())
                            ->setServiceProviderName($name)
                            ->setVoucherLimit($voucher_limit)
                            ->setServiceProviderID(bin2hex(openssl_random_pseudo_bytes(5)));

                    // Persist the Data
                    $this->em->persist($ServiceProvider);

                    // Check if the user_id is present or not.
                    $user = $this->em->getRepository('ProjectBundle:User')->find($user_id);

                    // Set the user id in the service provider.
                    $ServiceProvider -> setCreatedBy($user);

                    // Create the voucher based on the voucher limit.
                    for($i = 0; $i < $voucher_limit; $i++)
                    {
                           $voucher = $this->validate->PostVoucher($user,$ServiceProvider,date("Y-m-d", strtotime("+30 days")));
                    }

                    $this->em->flush();
                }
                else
                {
                    // If the record is invalid, store them in a temporary array and print them.
                    $temp[$i++]=array($name,$voucher_limit,$user_id);
                }
            }
        }

        catch(Exception $e)
        {
            throw new Exception($e->getMessage());
        }
	}
}