<?php
namespace Task\ProjectBundle\Controller\v1\Vouchers;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;
use \Exception as Exception;

/*
	Class to Create a new Service Provider.
*/
class FetchVouchers extends FOSRestController
{
    /**
     * @Rest\Get("/fetchVouchers")
     * Method to Fetch Vouchers
     */
    public function fetchVouchers(Request $request)
    {
        try {
            $user = $request->attributes->get('user_object');
            $em = $this->getDoctrine()->getManager();
            $vouchersRepository = $em->getRepository('ProjectBundle:Vouchers');
            $vouchers = $vouchersRepository->findAll();
            $size = sizeof($vouchers);
            $json_response = [];
            for($i=0; $i<$size; $i++) {
                $inner_json = array(
                    'id' => $vouchers[$i]->getVoucherId(),
                    'Status' => $vouchers[$i]->getStatus(),
                    'Expiry_Date' => $vouchers[$i]->getExpiryDate(),
                    'ServiceProvider' => $vouchers[$i]->getServiceProviderID()->getServiceProviderName(),
                    'Remark' => $vouchers[$i]->getRemark()
                );
                $json_response[]=$inner_json;
            }
            return array('status' => $json_response);
        } catch (\Exception $exception) {
            throw $exception;
        }
    }
}