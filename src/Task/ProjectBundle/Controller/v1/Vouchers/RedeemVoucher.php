<?php
namespace Task\ProjectBundle\Controller\v1\Vouchers;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;
use Task\ProjectBundle\Entity\Vouchers;


/*
	Class to redeem voucher.
*/
class RedeemVoucher extends FOSRestController 
{
   /**
 	* @Rest\Put("/redeem")
 	* Method to redeem voucher.
 	*/

 	public function redeemVouchers(Request $request)
 	{
 		$voucherID = $request->get('voucherID');
	 	$data = new Vouchers();
		$em = $this->getDoctrine()->getManager();

		// Call the validate service
		$validate = $this->get('validate');

		$user = $request->attributes->get('user_object');

		// Check whether the input voucher id is correct or not.
		$findVoucher = $validate->findVoucher($voucherID, $user);
		if($findVoucher == null) {
			return array("Error" => "No Such Voucher Present");
		}
			
		// Call the RedeemVoucher method inside the validate service to Redeem the new voucher
		return $validate->RedeemVoucher($findVoucher);
	}
}