<?php
namespace Task\ProjectBundle\Controller\v1\Vouchers;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;
use Task\ProjectBundle\Entity\Vouchers;

/*
	class to create voucher
*/
class CreateVoucher extends FOSRestController 
{
	 /**
	 * @Rest\Post("/vouchers")
	 * Method to Create new Voucher.
	 */

	public function CreateVouchers(Request $request)
	{
	 	// Get the parameter from the request.
		$expiry_date = $request->get('expiry_date');
        $service_provider_id = $request->get('service_provider_id');
        $remark = $request->get('remark');
		$user = $request->attributes->get('user_object');

		/*
			Check whether the parameters are not empty.
			If they are empty then return an error message.
		*/
		if(empty($expiry_date)) {
			return array("Error" => "Fields Cannot be Empty");
		}
		// Get the validate Service
		$validate = $this->get('validate');
			
		// Check whether the user has created the service provider or not.
		$ServiceProvider = $validate->validateServiceProvider($service_provider_id);
		if(!$ServiceProvider) {
		    throw new \Exception("Invalid Service Provider ID");
        }
		$limit = $ServiceProvider->getVoucherLimit();

		$count = $this->getDoctrine()
            ->getRepository(Vouchers::class)
            ->CountVouchers($ServiceProvider);
		if($limit == $count) {
			throw new \Exception("Voucher cannot be added. Limit exceeded.");
		}
		// Call the PostVoucher method inside the validate service to create the new voucher
		$status = $validate->PostVoucher($user, $ServiceProvider, $expiry_date, $remark);
		if($status) {
            return array("Success" => array("message"=>"Voucher Added Successfully","voucherID"=>$status->getVoucherId()));
        }
	}
}