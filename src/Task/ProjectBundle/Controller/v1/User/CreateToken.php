<?php
namespace Task\ProjectBundle\Controller\v1\User;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;

/*
	Class to Create Token
*/

class CreateToken extends FOSRestController 
{
	 /**
     * @Rest\Get("/GetToken")
     * Method to Generate a API token that will be used for authentication
     */
    public function generateToken(Request $request)
    {	
	    // Create a random string of 8 bytes and set as api_token.
		$token = bin2hex(openssl_random_pseudo_bytes(8));
		$user = $request->attributes->get('user_object');
		$user -> setApiToken($token);
		$this->getDoctrine()->getManager()->flush();
		return array("Token" => $token);					 
    }
}