<?php
namespace Task\ProjectBundle\Controller\v1\User;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;
use Task\ProjectBundle\Entity\User;

/*
	Class to Register
*/

class RegisterUser extends FOSRestController
{
    /**
     * @Rest\Post("/register")
     * Method to Generate a API token that will be used for authentication
     */
    public function registerUser(Request $request)
    {
        $username = $request->get('username');
        $email = $request->get('email');
        $password = $request->get('password');
        $em = $this->getDoctrine()->getManager();
        $userRepository = $em->getRepository('ProjectBundle:User');

        $exist = $userRepository->findOneBy(array('username'=>$username));
        if($exist) {
            throw new \Exception('User Exists');
        }

        $user = new User();
        $user->setUsername($username);
        $user->setPlainPassword($password);
        $user->setEmail($email);
        $user->setEnabled(true);
        $em->persist($user);
        $em->flush();
        return array('Status' => 'User Created');
    }
}