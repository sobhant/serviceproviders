<?php
namespace Task\ProjectBundle\Controller\v1\ServiceProvider;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;
use \Exception as Exception;

/*
	Class to Create a new Service Provider.
*/
class CreateServiceProvider extends FOSRestController 
{
	/**
	 * @Rest\Post("/serviceprovider")
	 * Method to Create new Service Provider.
	 */
	 public function PostServiceProvider(Request $request)
	 {
	 	try
	 	{
	 		// Collect the parameters from the request.
		 	$name = $request->get('name');
		   	$limit = $request->get('limit');

		   	$user = $request->attributes->get('user_object');

		   	// Check whether the parameters are not empty.
		   	// If they are empty then return an error message.
		 	if(empty($name) || empty($limit)) {
				return array("Error" =>"Fields Cannot be Empty");
		 	}
		 	if($limit > 20) {
		 	    throw new \Exception("Voucher limit cannot exceed 20");
            }
			$result = $this->get('validate')->PostServiceProvider($user,$name,$limit);
		 	$json_response = array(
		 	    'serviceProvider_ID' => $result->getServiceProviderID(),
                'serviceProvider_Name' => $result->getServiceProviderName(),
                'voucher_limit' => $result->getVoucherLimit()
            );
		 	if($result) {
                return array("Status" => $json_response);
            }
	 	}
	   	catch(Exception $e)
        {
            throw $e;
        }
	 }
}