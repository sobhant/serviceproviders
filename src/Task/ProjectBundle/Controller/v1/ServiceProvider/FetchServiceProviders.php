<?php
namespace Task\ProjectBundle\Controller\v1\ServiceProvider;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;
use \Exception as Exception;

/*
	Class to Create a new Service Provider.
*/
class FetchServiceProviders extends FOSRestController
{
    /**
     * @Rest\Get("/fetchServiceProviders")
     * Method to Create new Service Provider.
     */
    public function fetchServiceProviders(Request $request)
    {
        try {
            $user = $request->attributes->get('user_object');
            $em = $this->getDoctrine()->getManager();
            $serviceProviderRepository = $em->getRepository('ProjectBundle:ServiceProvider');
            $serviceProviders = $serviceProviderRepository->findAll(array('createdBy' => $user));
            $size = sizeof($serviceProviders);
            $json_response = [];
            for($i=0; $i<$size; $i++) {
                $inner_json = array(
                    'id' => $serviceProviders[$i]->getId(),
                    'ServiceProviderName' => $serviceProviders[$i]->getServiceProviderName(),
                    'ServiceProvider_id' => $serviceProviders[$i]->getServiceProviderID(),
                    'voucher_limit' => $serviceProviders[$i]->getVoucherLimit()

                );
                $json_response[]=$inner_json;
            }
            return array('status' => $json_response);
        } catch (\Exception $exception) {
            throw $exception;
        }

    }
}