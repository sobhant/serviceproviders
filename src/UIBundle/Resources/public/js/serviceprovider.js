$(document).ready(function () {
    $.ajax
    ({
        type: "GET",
        url: "v1/api/fetchServiceProviders",
        dataType: 'json',
        beforeSend: function (xhr) {
            xhr.setRequestHeader('api_token', localStorage.getItem("token"));
        },
        success: function (result) {
            let status = result['status'];
            let len = status.length;
            if (len === 0) {
                let err = $("#SPError");
                err.html("No Service Provider Present. Fill the form and Click on Add.");
                err.css("background", "#bbdefb");
                err.css("padding", "10px");
                err.css("width", "100%");
                $(".sp-delete-row").hide();
                return;
            }
            for (i = 0; i < len; i++) {
                let markup = "<tr><td><input type='checkbox' name='record'></td><td>" + status[i]['ServiceProvider_id'] + "</td><td>" + status[i]['ServiceProviderName'] + "</td><td>" + status[i]['voucher_limit'] + "</td></tr>";
                $("#SpTable #SpTbody").append(markup);
            }
        },
        error: function (result) {
            console.log("error");
        }
    });

    $(".add-row").click(function () {
        let name = $("#name").val();
        let limit = $("#limit").val();
        if(name === '' || limit === '') {
            $("#SPError").show();
            $("#SPError").html("Fields cannot be blank");
            return;
        }
        let data = {
            "name": name,
            "limit": limit
        };
        $.ajax
        ({
            type: "POST",
            url: "v1/api/serviceprovider",
            dataType: 'json',
            data: data,
            beforeSend: function (xhr) {
                xhr.setRequestHeader('api_token', localStorage.getItem("token"));
            },
            success: function (result) {
                $("#SPError").hide();
                $("#SpTable").show();
                $(".sp-delete-row").show();
                $("#success").html("Service Provider Added");
                let markup = "<tr><td><input type='checkbox' name='record'></td><td>" + result['Status']['serviceProvider_ID'] + "</td><td>" + result['Status']['serviceProvider_Name'] + "</td><td>" + result['Status']['voucher_limit'] + "</td></tr>";
                $("#SpTable #SpTbody").append(markup);
                bootbox.alert("Service Provider Added");
            },
            error: function (result) {
                console.log(result['responseJSON']['Error']);
                $("#SPError").show();
                $("#SPError").html(result['responseJSON']['Error']);
            }
        });
    });

    // Find and remove selected table rows
    $(".sp-delete-row").click(function () {
        $("table tbody").find('input[name="record"]').each(function () {
            if ($(this).is(":checked")) {
                $(this).parents("tr").remove();
                // console.log($(this).parents("tr"));
            }
        });
    });
});