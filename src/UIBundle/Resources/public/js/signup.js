$(document).ready(function(){
    $("button").click(function(e){
        e.preventDefault();
        let username = $("#username").val();
        let password = $("#pwd").val();
        let repeatPassword = $("#cnf-pwd").val();
        let email = $("#email").val();
        if(username === '' || password === '' || email === '') {
            $("#error").html("All Fields are mandatory");
            return;
        }
        if(password !== repeatPassword) {
            $("#error").html("Passwords Do Not Match");
            return;
        }
        if(password.length <= 8) {
            $("#error").html("Password length cannot be less than 8");
            return;
        }
        let jsonData = {
            "username" : username,
            "password" : password,
            "email" : email
        };
        $.ajax
        ({
            type: "POST",
            url: "v1/api/register",
            dataType: 'json',
            data: jsonData,
            success: function (result){
                window.location.href = 'login';
            },
            error: function (result) {
                $("#error").html("User Already Exists");
            }
        });
    });
});