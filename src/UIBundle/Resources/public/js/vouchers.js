$(document).ready(function () {
    var sp = '';
    $.ajax
    ({
        type: "GET",
        url: "v1/api/fetchVouchers",
        dataType: 'json',
        beforeSend: function (xhr) {
            xhr.setRequestHeader('api_token', localStorage.getItem("token"));
        },
        success: function (result) {
            let status = result['status'];
            let len = status.length;
            if (len === 0) {
                // let err = $("#error");
                // err.html("No Service Provider Present. Fill the form and Click on Add.");
                // err.css("background", "#bbdefb");
                // err.css("padding", "10px");
                // err.css("width", "100%");
                // $(".delete-row").hide();
                // return;
            }
            for (i = 0; i < len; i++) {
                let markup = "<tr><td><input type='checkbox' name='record'></td><td>" + status[i]['id'] + "</td><td>" + status[i]['Status'] + "</td><td>" + status[i]['Expiry_Date'] + "</td><td>" + status[i]['ServiceProvider'] +"</td><td>" + status[i]['Remark'] +"</td></tr>";
                $("#VTable #VTbody").append(markup);
            }
        },
        error: function (result) {
            console.log(result);
        }
    });
    $(function(){
        $(".dropdown-menu").on('click', 'li a', function(){
            $("#spbutton:first-child").text($(this).text());
            $("#spbutton:first-child").val($(this).text());
            sp = ($(this).text());
        });
    });

    $("#modalclick").click(function (e) {
        $("#modalerror").hide();
        $("#datefield").html("");
        $("#remarkfield").html("");
        $('#datetimepicker1').hide();
    });
    $("#addvoucher").click(function (e) {
        let date= $("#datefield").val();
        let serviceprovider = sp;
        let remark = $("#remarkfield").val();
        if(remark ==='' || date === '' || serviceprovider === '') {
            $("#modalerror").show();
            $("#modalerror").css("color","red");
            $("#modalerror").html("All Fields are mandatory");
            return;
        }
        let markup = "<tr><td><input type='checkbox' name='record'></td><td>" + "abcd1234" + "</td><td>" + "active" + "</td><td>" + date + "</td><td>" + sp +"</td><td>" + remark +"</td></tr>";
        $("#VTable #VTbody").append(markup);
        $('#exampleModal').modal('toggle');
    });

    $("#datefield").click(function () {
        $('#datetimepicker1').show();
        $(function() {
            $('#datetimepicker1').datepicker({
                onSelect: function(dateText, inst) {
                    var dateAsString = dateText; //the first parameter of this function
                    var dateAsObject = $(this).datepicker( 'getDate' ); //the getDate method
                    $("#datefield").val(dateAsString);
                    $('#datetimepicker1').hide();
                }
            });
        });
    });

});