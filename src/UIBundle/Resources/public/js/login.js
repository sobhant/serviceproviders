$(document).ready(function(){
    $(".login100-form-btn").click(function(e){
        e.preventDefault();
        var username = $("#input-username").val();
        var password = $("#input-password").val();
        if(username === '' || password === '') {
            $(".blinking").html("Fields cannot be blank");
            return;
        }
        $.ajax
        ({
            type: "GET",
            url: "v1/api/GetToken",
            dataType: 'json',
            headers: {
                "Authorization": "Basic " + btoa(username + ":" + password)
            },
            success: function (result){
                $("#InvalidCredentials").html("");
                let token = JSON.parse(JSON.stringify(result)).Token;
                localStorage.setItem("token", token);
                window.location.href = 'serviceproviders';
            },
            error: function (result) {
                $(".blinking").html("Invalid Credentials");
            }
        });
    });
});

$('.js-tilt').tilt({
    scale: 1.1
});