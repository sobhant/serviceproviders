<?php
/**
 * Created by PhpStorm.
 * User: minfire
 * Date: 29/1/19
 * Time: 4:40 PM
 */

namespace UIBundle\Controller\ServiceProvider;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
class ServiceProviderController extends Controller
{
    /**
     * @Route("/serviceproviders", name="serviceproviders")
     */
    public function indexAction()
    {
        return $this->render('UIBundle:ServiceProviders:ServiceProviders.html.twig');
    }

}