<?php
/**
 * Created by PhpStorm.
 * User: minfire
 * Date: 29/1/19
 * Time: 4:40 PM
 */

namespace UIBundle\Controller\Users;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
class SecurityController extends Controller
{
    /**
     * @Route("/login", name="login")
     */
    public function loginAction()
    {
        return $this->render('UIBundle:Users:login.html.twig');
    }

    /**
     * @Route("/forgot", name="forgot_password")
     */
    public function forgotAction()
    {
        echo "Forgot Password";
        die();
    }

    /**
     * @Route("/signup", name="signup")
     */
    public function registerAction()
    {
        return $this->render('UIBundle:Users:signup.html.twig');
    }

}